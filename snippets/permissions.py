from rest_framework import permissions


class IsOwnerOrReadeOnly(permissions.BasePermission):
    """
    Custom Permission
    """

    def has_object_permission(self, request, view, obj):
        # Solicitações de leitura são permitidas a qualquer pedido
        if request.method in permissions.SAFE_METHODS:
            return True
        # Gravação só é permitida ao dono do snippet
        return obj.owner == request.user
